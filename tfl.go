// tfl is a Transport for London API client package.
//
// It implements various types and functions to easily work with tfl API responses, and convert
// statuses to English language phrases.
//
// It currently only works for tube (Underground) objects
//
// More information about the tfl API be found at https://api.tfl.gov.uk/
package tfl

import (
	"log"
	"strings"
)

// Line represents a Tube Line object returned from the Transport for London API.
type Line struct {
	Name         string
	LineStatuses []LineStatus
}

// LineStatus represents a Tube Line Status object, included in a Line object.
type LineStatus struct {
	StatusSeverityDescription string
	Reason                    string
}

// Lines is an array of Line
type Lines []Line

// GetTubeLineFromRequestPhrase extracts a Tube Line name from a request phrase.
//
// If the input is "Victoria line", this function returns "victoria".
//
// If the input is "all lines", this function returns "all".
func GetTubeLineFromRequestPhrase(inStr string) string {
	log.Printf("Debug: ++getTubeLineFromRequestPhrase()")
	defer log.Printf("Debug: --getTubeLineFromRequestPhrase()")

	// remove unneeded words from the request phrase
	r := strings.TrimSpace(strings.Replace(inStr, "lines", "", -1))
	r = strings.TrimSpace(strings.Replace(r, "Lines", "", -1))
	r = strings.TrimSpace(strings.Replace(r, "line", "", -1))
	r = strings.TrimSpace(strings.Replace(r, "Line", "", -1))
	r = strings.TrimSpace(strings.Replace(r, "check", "", -1))
	r = strings.TrimSpace(strings.Replace(r, "Check", "", -1))
	r = strings.TrimSpace(strings.Replace(r, "tube", "", -1))
	r = strings.TrimSpace(strings.Replace(r, "Tube", "", -1))
	r = strings.TrimSpace(strings.Replace(r, "ask", "", -1))
	r = strings.TrimSpace(strings.Replace(r, "Ask", "", -1))
	r = strings.TrimSpace(strings.Replace(r, "about", "", -1))
	r = strings.TrimSpace(strings.Replace(r, "About", "", -1))

	// replacing "and" with "&" to match TfL naming format
	r = strings.TrimSpace(strings.Replace(r, "and", "&", -1))
	r = strings.TrimSpace(strings.Replace(r, "And", "&", -1))

	// set everything to lowercase
	r = strings.ToLower(r)

	return r
}

// ToMapByLineName creates a map of Line data based on Line name.
//
// This function makes it easy to look up Line data based on the Line name without having to manually iterate
// over an array of Lines.
func (lines Lines) ToMapByLineName() map[string]Line {
	log.Printf("Debug: ++ToMapByLineName()")
	defer log.Printf("Debug: --ToMapByLineName()")

	theMap := make(map[string]Line)

	for _, line := range lines {
		theMap[strings.ToLower(line.Name)] = line
	}

	return theMap
}

// ToMapByStatus creates a map of Line(s) data based on Line status.
//
// This function makes it easy to look up one or more Lines based on their status.
func (lines Lines) ToMapByStatus() map[string]Lines {
	log.Printf("Debug: ++ToMapByStatus()")
	defer log.Printf("Debug: --ToMapByStatus()")

	theMap := make(map[string]Lines)

	for _, line := range lines {
		theMap[line.LineStatuses[0].StatusSeverityDescription] = append(theMap[line.LineStatuses[0].StatusSeverityDescription], line)
	}

	return theMap
}

// GetPhraseForAllLines formats a response string based on multiple Lines of varying statuses.
func GetPhraseForAllLines(lines Lines) string {
	log.Printf("Debug: ++getPhraseForAllLines()")
	defer log.Printf("Debug: --getPhraseForAllLines()")

	var ret string

	statuses := lines.ToMapByStatus()
	statusExceptions := 0

	for status, lines := range statuses {
		if status != "Good Service" {
			statusExceptions++
			phrase := GetPhraseForLinesStatus(lines)
			if len(ret) == 0 {
				ret = phrase
			} else {
				ret = ret + " " + phrase
			}
		}
	}

	if statusExceptions == 0 {
		ret = "All lines are operating a good service."
	} else {
		ret = ret + " All other lines are operating a good service."
	}

	return ret
}

// GetPhraseForStatus formats a response string for a single Line.
func GetPhraseForStatus(line Line) string {
	log.Printf("Debug: ++getPhraseForStatus()")
	defer log.Printf("Debug: --getPhraseForStatus()")

	ret := GetPhraseForLinesStatus(Lines{line})

	if len(line.LineStatuses[0].Reason) > 0 {
		ret = ret + " " + RemoveLineIntroFromReason(line.LineStatuses[0].Reason)
	}

	return ret
}

// GetPhraseForLinesStatus formats a response string for one or more Lines having
// the same status.
// This function assumes that all Lines in `lines` have the same status.
func GetPhraseForLinesStatus(lines Lines) string {
	log.Printf("Debug: ++getPhraseForLinesStatus()")
	defer log.Printf("Debug: --getPhraseForLinesStatus()")

	var linePhrase string
	var isAre string
	var lineFragment string
	var ret string

	if len(lines) == 1 {
		isAre = "is"
		linePhrase = lines[0].Name + " line"
	} else {
		isAre = "are"
		for i, line := range lines {
			if i == 0 {
				linePhrase = line.Name
				continue
			}
			if i == len(lines)-1 {
				lineFragment = "and " + line.Name
			} else {
				lineFragment = line.Name
			}
			commaOrNot := ""
			if len(lines) > 2 {
				commaOrNot = ","
			}
			linePhrase = linePhrase + commaOrNot + " " + lineFragment
		}

		linePhrase = linePhrase + " lines"
	}

	status := lines[0].LineStatuses[0].StatusSeverityDescription

	switch status {
	case "Special Service":
		ret = "The " + linePhrase + " " + isAre + " operating a special service."
	case "Closed":
		ret = "The " + linePhrase + " " + isAre + " closed."
	case "Suspended":
		ret = "The " + linePhrase + " " + isAre + " suspended."
	case "Part Suspended":
		ret = "The " + linePhrase + " " + isAre + " part suspended."
	case "Planned Closure":
		ret = "There are planned closures on the " + linePhrase + "."
	case "Part Closure":
		ret = "There are part closures on the " + linePhrase + "."
	case "Severe Delays":
		ret = "There are severe delays on the " + linePhrase + "."
	case "Reduced Service":
		ret = "The " + linePhrase + " " + isAre + " operating with reduced service."
	case "Bus Service":
		ret = "The " + linePhrase + " " + isAre + " operating with bus service."
	case "Minor Delays":
		ret = "There are minor delays on the " + linePhrase + "."
	case "Good Service":
		ret = "The " + linePhrase + " " + isAre + " operating a good service."
	case "Part Closed":
		ret = "The " + linePhrase + " " + isAre + " part closed."
	case "Exit Only":
		ret = "The " + linePhrase + " " + isAre + " currently in an exit only state."
	case "No Step Free Access":
		ret = "There is no step free access on the " + linePhrase + "."
	case "Change of frequency":
		ret = "There is a change of frequency on the " + linePhrase + "."
	case "Diverted":
		ret = "The " + linePhrase + " " + isAre + " currently diverted."
	case "Not Running":
		ret = "The " + linePhrase + " " + isAre + " currently not running."
	case "Issues Reported":
		ret = "There are issues reported on the " + linePhrase + "."
	case "No Issues":
		ret = "There are no issues reported on the " + linePhrase + "."
	case "Information":
		ret = "There is some information about the " + linePhrase + "."
	case "Service Closed":
		ret = "The " + linePhrase + " " + isAre + " closed."
	default:
		log.Printf("Warning: found unexpected TfL SeverityDescription: %s", status)
		ret = "The status of the " + linePhrase + " " + isAre + ": " + status + "."
	}

	return ret
}

// RemoveLineIntroFromReason strips the Line intro from the front of a `reason`
// object returned from the Transport for London API.
//
// Example: if TFL's `reason` is "Victoria Line: closed until tomorrow morning.",
// this function returns "closed until tomorrow morning."
//
// Used for formatting response strings in GetPhraseForStatus() without being redundant.
func RemoveLineIntroFromReason(reason string) string {
	start := strings.Index(reason, ": ") + 2
	return reason[start:]
}
