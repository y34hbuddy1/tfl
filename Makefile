CC=go build
SOURCES=tfl.go
LIBRARY=tfl.a

all: $(SOURCES) $(LIBRARY)

$(LIBRARY): $(SOURCES)
	go test -cover
	GOOS=linux $(CC) $(SOURCES)

install:
	GOOS=linux go install

clean:
	@rm -fv $(LIBRARY)
