package tfl

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func NewTflLine(Name string, StatusSeverityDescription string, Reason string) Line {
	return Line{
		Name: Name,
		LineStatuses: []LineStatus{
			{
				StatusSeverityDescription: StatusSeverityDescription,
				Reason: Reason,
			},
		},
	}
}

func TestGetTubeLineFromRequestPhrase(t *testing.T) {
	tables := []struct {
		in, want string
	}{
		{"Victoria line", "victoria"},
		{"all lines", "all"},
		{"negative test", "negative test"},
		{"hammersmith and city line", "hammersmith & city"},
		{"Hammersmith & City Line", "hammersmith & city"},
		{"", ""},
	}

	for _, table := range tables {
		got := GetTubeLineFromRequestPhrase(table.in)
		assert.Equal(t, table.want, got)
	}
}

func TestTflLines_ToMapByLineName(t *testing.T) {
	line1 := NewTflLine("Victoria", "Good Service", "")
	line2 := NewTflLine("Northern", "Minor Delays", "Minor Delays due to earlier signal failure")
	line3 := NewTflLine("Circle", "Part Closed", "Part Closed due to security incident")

	want := make(map[string]Line)
	want["victoria"] = line1
	want["northern"] = line2
	want["circle"] = line3

	lines := Lines{line1, line2, line3}

	got := lines.ToMapByLineName()

	assert.Equal(t, want, got)
}

func TestTflLines_ToMapByStatus(t *testing.T) {
	line1 := NewTflLine("Victoria", "Good Service", "")
	line2 := NewTflLine("Northern", "Good Service", "")
	line3 := NewTflLine("Circle", "Part Closed", "Part Closed due to security incident")
	line4 := NewTflLine("District", "Severe Delays", "Severe Delays due to broken train")

	lines := Lines{line1, line2, line3, line4}

	want := make(map[string]Lines)
	want["Good Service"] = append(want["Good Service"], line1)
	want["Good Service"] = append(want["Good Service"], line2)
	want["Part Closed"] = append(want["Part Closed"], line3)
	want["Severe Delays"] = append(want["Severe Delays"], line4)

	got := lines.ToMapByStatus()

	assert.Equal(t, want, got)
}

func TestGetPhraseForLinesStatus(t *testing.T) {
	tables := []struct {
		in   Lines
		want string
	}{
		{Lines{
			NewTflLine("Victoria", "Good Service", ""),
			NewTflLine("Northern", "Good Service", ""),
			NewTflLine("Circle", "Good Service", ""),
		},
			"The Victoria, Northern, and Circle lines are operating a good service.",
		},
		{Lines{
			NewTflLine("Bakerloo", "Closed", "The Bakerloo line is closed."),
		},
			"The Bakerloo line is closed.",
		},
		{
			Lines{
				NewTflLine("Victoria", "Special Service", ""),
			}, "The Victoria line is operating a special service."},
		{Lines{
			NewTflLine("Victoria", "Suspended", ""),
			NewTflLine("Northern", "Suspended", ""),
			NewTflLine("Circle", "Suspend", ""),
		},
			"The Victoria, Northern, and Circle lines are suspended.",
		},
		{Lines{
			NewTflLine("Victoria", "Part Suspended", ""),
			NewTflLine("Northern", "Part Suspended", ""),
			NewTflLine("Circle", "Part Suspend", ""),
		},
			"The Victoria, Northern, and Circle lines are part suspended.",
		},
		{Lines{
			NewTflLine("Victoria", "Planned Closure", ""),
			NewTflLine("Northern", "Planned Closure", ""),
			NewTflLine("Circle", "Planned Closure", ""),
		},
			"There are planned closures on the Victoria, Northern, and Circle lines.",
		},
		{Lines{
			NewTflLine("Victoria", "Part Closure", ""),
			NewTflLine("Northern", "Part Closure", ""),
			NewTflLine("Circle", "Part Closure", ""),
		},
			"There are part closures on the Victoria, Northern, and Circle lines.",
		},
		{Lines{
			NewTflLine("Victoria", "Severe Delays", ""),
			NewTflLine("Northern", "Severe Delays", ""),
			NewTflLine("Circle", "Severe Delays", ""),
		},
			"There are severe delays on the Victoria, Northern, and Circle lines.",
		},
		{Lines{
			NewTflLine("Victoria", "Reduced Service", ""),
			NewTflLine("Northern", "Reduced Service", ""),
			NewTflLine("Circle", "Reduced Service", ""),
		},
			"The Victoria, Northern, and Circle lines are operating with reduced service.",
		},
		{Lines{
			NewTflLine("Victoria", "Bus Service", ""),
			NewTflLine("Northern", "Bus Service", ""),
			NewTflLine("Circle", "Bus Service", ""),
		},
			"The Victoria, Northern, and Circle lines are operating with bus service.",
		},
		{Lines{
			NewTflLine("Victoria", "Minor Delays", ""),
			NewTflLine("Northern", "Minor Delays", ""),
			NewTflLine("Circle", "Minor Delays", ""),
		},
			"There are minor delays on the Victoria, Northern, and Circle lines.",
		},
		{Lines{
			NewTflLine("Victoria", "Part Closed", ""),
			NewTflLine("Northern", "Part Closed", ""),
			NewTflLine("Circle", "Part Closed", ""),
		},
			"The Victoria, Northern, and Circle lines are part closed.",
		},
		{Lines{
			NewTflLine("Victoria", "Exit Only", ""),
			NewTflLine("Northern", "Exit Only", ""),
			NewTflLine("Circle", "Exit Only", ""),
		},
			"The Victoria, Northern, and Circle lines are currently in an exit only state.",
		},
		{Lines{
			NewTflLine("Victoria", "No Step Free Access", ""),
			NewTflLine("Northern", "No Step Free Access", ""),
			NewTflLine("Circle", "No Step Free Access", ""),
		},
			"There is no step free access on the Victoria, Northern, and Circle lines.",
		},
		{Lines{
			NewTflLine("Victoria", "Change of frequency", ""),
			NewTflLine("Northern", "Change of frequency", ""),
			NewTflLine("Circle", "Change of frequency", ""),
		},
			"There is a change of frequency on the Victoria, Northern, and Circle lines.",
		},
		{Lines{
			NewTflLine("Victoria", "Diverted", ""),
			NewTflLine("Northern", "Diverted", ""),
			NewTflLine("Circle", "Diverted", ""),
		},
			"The Victoria, Northern, and Circle lines are currently diverted.",
		},
		{Lines{
			NewTflLine("Victoria", "Not Running", ""),
			NewTflLine("Northern", "Not Running", ""),
			NewTflLine("Circle", "Not Running", ""),
		},
			"The Victoria, Northern, and Circle lines are currently not running.",
		},
		{Lines{
			NewTflLine("Victoria", "Issues Reported", ""),
			NewTflLine("Northern", "Issues Reported", ""),
			NewTflLine("Circle", "Issues Reported", ""),
		},
			"There are issues reported on the Victoria, Northern, and Circle lines.",
		},
		{Lines{
			NewTflLine("Victoria", "No Issues", ""),
			NewTflLine("Northern", "No Issues", ""),
			NewTflLine("Circle", "No Issues", ""),
		},
			"There are no issues reported on the Victoria, Northern, and Circle lines.",
		},
		{Lines{
			NewTflLine("Victoria", "Information", ""),
			NewTflLine("Northern", "Information", ""),
			NewTflLine("Circle", "Information", ""),
		},
			"There is some information about the Victoria, Northern, and Circle lines.",
		},
		{Lines{
			NewTflLine("Victoria", "Service Closed", ""),
			NewTflLine("Northern", "Service Closed", ""),
			NewTflLine("Circle", "Service Closed", ""),
		},
			"The Victoria, Northern, and Circle lines are closed.",
		},
		{Lines{
			NewTflLine("Victoria", "Nonexistent Status", ""),
			NewTflLine("Northern", "Nonexistent Status", ""),
			NewTflLine("Circle", "Nonexistent Status", ""),
		},
			"The status of the Victoria, Northern, and Circle lines are: Nonexistent Status.",
		},
		{Lines{
			NewTflLine("Victoria", "Closed", ""),
			NewTflLine("Northern", "Closed", ""),
		},
			"The Victoria and Northern lines are closed.",
		},
	}

	for _, table := range tables {
		got := GetPhraseForLinesStatus(table.in)
		assert.Equal(t, table.want, got)
	}
}

func TestGetPhraseForStatus(t *testing.T) {
	tables := []struct {
		in   Line
		want string
	}{
		{NewTflLine("Victoria", "Good Service", ""),
			"The Victoria line is operating a good service."},
		{NewTflLine("Northern", "Severe Delays", "Northern Line: Severe delays due to signal failure."),
			"There are severe delays on the Northern line. Severe delays due to signal failure."},
	}

	for _, table := range tables {
		got := GetPhraseForStatus(table.in)
		assert.Equal(t, table.want, got)
	}
}

func TestGetPhraseForAllLines(t *testing.T) {
	tables := []struct {
		in   Lines
		want string
	}{
		{Lines{
			NewTflLine("Victoria", "Good Service", ""),
			NewTflLine("Northern", "Good Service", ""),
			NewTflLine("Circle", "Good Service", ""),
		},
			"All lines are operating a good service.",
		},
		{Lines{
			NewTflLine("Victoria", "Good Service", ""),
			NewTflLine("Northern", "Good Service", ""),
			NewTflLine("Circle", "Good Service", ""),
			NewTflLine("District", "Minor Delays", "Minor delays due to signal failure"),
		},
			"There are minor delays on the District line. All other lines are operating a good service.",
		},
		{Lines{
			NewTflLine("Victoria", "Good Service", ""),
			NewTflLine("Northern", "Good Service", ""),
			NewTflLine("Circle", "Good Service", ""),
			NewTflLine("District", "Minor Delays", "Minor delays due to signal failure"),
			NewTflLine("Jubilee", "Severe Delays", "Severe delays due to signal failure"),
		},
			"There are minor delays on the District line. There are severe delays on the Jubilee line. All other lines are operating a good service.",
		},
	}

	for _, table := range tables {
		got := GetPhraseForAllLines(table.in)
		assert.Equal(t, table.want, got)
	}
}

func TestRemoveLineIntroFromReason(t *testing.T) {
	tables := []struct {
		in   string
		want string
	}{
		{"Victoria Line: Minor delays due to signal failure.",
			"Minor delays due to signal failure.",
		},
		{
			"Northern Line: Severe delays due to signal failure.",
			"Severe delays due to signal failure.",
		},
	}

	for _, table := range tables {
		got := RemoveLineIntroFromReason(table.in)
		assert.Equal(t, table.want, got)
	}
}
